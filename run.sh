#!/bin/bash

# A single line of bash script to compile, package and run the java project
mvn compile && mvn package && java -jar target/SpringBootHelloWorld-0.0.1-SNAPSHOT.jar
